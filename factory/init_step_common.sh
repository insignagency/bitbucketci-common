#!/usr/bin/env bash
err_report() {
  echo "Error on line $1"
  display_files_content
}
trap 'err_report $LINENO' ERR

function display_files_content() {
  track "Affichage du contenu de chaque fichier de la pipeline pour debug"
  cd $orig_wd
  find pipelines -type f -exec echo "" \; -exec ls {} \; -exec cat {} \;
}
orig_wd=$(pwd)
. /pipelines/common.sh
### OPTIONS ###
while getopts c:b: opt; do
  case "$opt" in
  b)
    app_step_option="-b $OPTARG"
    ;;
  c)
    deploy_validate=1
    validate_envs=$OPTARG
    ;;
  \?) exit 1 ;; esac
done
shift $(($OPTIND - 1))
### OPTIONS ###
track "Exécution de /pipelines/set_auth.sh"
/pipelines/set_auth.sh
# script situé dans l'image enfant, contient le spécifique concernant la techno courante (magento2, drupal8 etc.)
/factory/init_step_app.sh $app_step_option

# Si on est dans le container jamstack on écrit une conversion des pm2-deploy.config.js en host.php
if [ "$TECHNO" == "jamstack" ]; then
  transpose_envs=$(ls -1 pipelines/deploy/ | grep -Ev "misc|default")
  for transpose_env in $transpose_envs; do
    if [ -f "pipelines/deploy/$transpose_env/pm2-deploy.config.js" ]; then
      node pipelines/deploy/misc/pm2-config-to-phphost.js $transpose_env > pipelines/deploy/$transpose_env/host.php
    fi
  done
fi

if (($deploy_validate)); then
  track "Validation des éléments contenus dans le répertoire 'pipelines' des sources git"
  if [ -f "/tmp/pipelines-validated.flag" ]; then
    exit 0
  fi

  if [ ! -d "pipelines/deploy" ]; then
    track "Répertoire pipelines/deploy inexistant"
    exit 1
  fi
  if [ ! -d "pipelines/deploy/production" ]; then
    track "Répertoire pipelines/deploy/production inexistant"
    if [ -d "pipelines/deploy/prod" ]; then
      track "Votre environnement correspondant à la production se nomme prod. Il serait mieux de le renommer production à l'occasion car ce nom deviendra un standard dans les CI Insign."
    fi
  fi

  cd pipelines/deploy/misc

  for deploy_env in $validate_envs; do
    if [ "$deploy_env" == "default" ]; then continue; fi
    if [ ! -f "../$deploy_env/host.php" ]; then
      track "Problème : pipelines/deploy/$deploy_env ne contient pas de fichier host.php"
      exit 1
    fi

    host=""
    stage=""

    infos="$(php get-host-info.php $deploy_env)"
    if [ "$?" -gt 0 ]; then
      track "Problème sur "$deploy_env" :"$infos
      exit 1
    fi
    # on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
    for var_exp in $infos; do
        if [[ ! "$var_exp" =~ .+/.+= ]]; then
        eval $var_exp
      fi
    done

    host_check="$(echo exit| telnet $host $port 2>&1)"
    if [[ ! "$host_check" =~ "Connection closed by foreign host" ]]; then
      track "Le host $host n'existe pas ou n'est pas un host extérieur: $host_check"
      exit 1
    fi
    if [ "$stage" == "" ]; then
      track "Aucune propriété 'stage' n'est définie dans deploy/$deploy_env/host.php."
      exit 1
    fi

  done

  track "Eléments de pipelines ok."
  cd ../../../
fi
# display_files_content
