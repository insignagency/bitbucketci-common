<?php
#
#  Ce fichier provient de l'image docker bitbucketci-common
#
require("/vendor/autoload.php");
use Symfony\Component\Yaml\Yaml;

$yml=file_get_contents("docker-compose.yml");
$yml=dc_format_input($yml);
$ymlArray = Yaml::parse($yml);

function transpose() {
    global $ymlArray;
    
    $volumeNode = "ref-volumes";
    foreach ($ymlArray["services"] as $serviceName => $service) {
      // Déplacement des volumes
      if (array_key_exists($volumeNode, $service)) {
        foreach ($service[$volumeNode] as $key => $volume) {
            $ymlArray["services"][$serviceName][$volumeNode][$key] = str_replace("~/", "", $volume);
            $ymlArray["services"][$serviceName][$volumeNode][$key] = preg_replace("|^([-a-z]+)|m", "./$1", $ymlArray["services"][$serviceName][$volumeNode][$key]);
        }
      } elseif (array_key_exists("volumes", $service) && is_array($service["volumes"])) {
        foreach ($service["volumes"] as $key => $volume) {
            $ymlArray["services"][$serviceName]["volumes"][$key] = str_replace("~/", "", $volume);
            $ymlArray["services"][$serviceName]["volumes"][$key] = preg_replace("|^([-a-z]+)|m", "./$1", $ymlArray["services"][$serviceName]["volumes"][$key]);
        }
        
      }
      // Ajout des ports d'écoute pour apache
      if($serviceName == "apache"){
          if (! array_key_exists("ports", $service)) {
            $ymlArray["services"][$serviceName]["ports"][] = "80:80";
            $ymlArray["services"][$serviceName]["ports"][] = "443:443";
          }

      }
    }

    $yml = Yaml::dump($ymlArray, 6, 1);
    
    $yml = dc_format_output($yml);
    echo $yml;
    
}
// pour que le parser ne développe pas les références et pointeurs du yml
function dc_format_input($ymlContent) {
    $ymlContent=str_replace("volumes: &appvolumes", "ref-volumes:", $ymlContent);
    $ymlContent=str_replace("*appvolumes", "pointer-appvolumes", $ymlContent);

    return $ymlContent;
}
function dc_format_output($ymlContent) {
    $ymlContent=str_replace("ref-volumes:", "volumes: &appvolumes", $ymlContent);
    $ymlContent=str_replace("pointer-appvolumes", "*appvolumes", $ymlContent);

    return $ymlContent;
}

transpose();