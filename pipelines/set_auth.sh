#!/usr/bin/env bash
if [ ! -d "/root/.composer/" ]; then mkdir ~/.composer/; fi
if [ "$COMPOSER_AUTH" == "" ]; then
  echo "Error : no composer auth given :"$COMPOSER_AUTH
  exit 1
fi
echo "$COMPOSER_AUTH" > ~/.composer/auth.json
echo -e "Host *\nStrictHostKeyChecking no" >> ~/.ssh/config