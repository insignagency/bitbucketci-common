<?php
/*
## Installing

Require googlechat recipe in your `deploy.php` file:

```php
require 'contrib/googlechat.php';
```

Add hook on deploy:

```php
before('deploy', 'googlechat:notify');
```

## Configuration

- `googlechat_webhook` – Google chat incoming webhook, **required**
   ex : 'https://chat.googleapis.com/v1/spaces/xxx/messages?key=xxx'

- `googlechat_notify_text` – notification message template, markdown supported, default:
  ```markdown
  ⌛ *{{user}}* is deploying branch `{{branch}}` to _{{target}}_
  ```
- `googlechat_success_text` – success template, default:
  ```markdown
  ✅ Branch `{{branch}}` deployed to _{{target}}_ successfully
  ```
- `googlechat_failure_text` – failure template, default:
  ```markdown
  ❌ Branch `{{branch}}` has failed to deploy to _{{target}}_
  ```
- `googlechat_buttons` – buttons
  ex : [
    [ 'url' => 'https://google.fr', 'text' => 'Go to google', 'display_on' => ['error'] ],
    [ 'url' => 'https://my-beautiful.website', 'text' => 'Go to google', 'display_on' => ['success']],
    [ 'url' => 'https://continuous.integration', 'text' => 'See details', 'display_on' => ['notify', 'success']]
  ]


## Usage

If you want to notify only about beginning of deployment add this line only:

```php
before('deploy', 'googlechat:notify');
```

If you want to notify about successful end of deployment add this too:

```php
after('success', 'googlechat:notify:success');
```

If you want to notify about failed deployment add this too:

```php
after('deploy:failed', 'googlechat:notify:failure');
```

If you want to add "buttons" to card :
```php
set('googlechat_buttons', function() {
  return [
    [
      'url' => 'https://my.website',
      'text' => 'Visit https://my.website',
      'display_on' => ['success'],
    ],
    [
      'url' => 'https://continuous.integration/xxx',
      'text' => 'See CI details',
      'display_on' => ['notify', 'success', 'failure'],
    ]
  ];
});
```
 */
namespace Deployer;

use Deployer\Utility\Httpie;
date_default_timezone_set('Europe/Paris');
// Deploy messages
set('bitbucket_tag',function () {
  $tag = runLocally('echo $BITBUCKET_TAG');
  return $tag;
});
set('build_link',function () {
  $repo_url = "https://bitbucket.org/" . runLocally('echo $BITBUCKET_REPO_FULL_NAME');
  $build_id = runLocally('echo $BITBUCKET_PIPELINE_UUID');
  $build_slug = "addon/pipelines/home#!/results/" . $build_id;
  return $repo_url . "/" . $build_slug;
});

set('googlechat_notify_text',function () {
  $msg = "";
  if (parse("{{bitbucket_tag}}")) {
    $msg = '⌛ *{{user}}* is deploying tag *{{bitbucket_tag}}* to *{{target}}*';
  } else {
    $msg = '⌛ *{{user}}* is deploying branch *{{branch}}* to *{{target}}*'; 
  }
  return $msg . ": {{build_link}}";
});
set('googlechat_success_text',function () {
  $msg = "";
  if (parse("{{bitbucket_tag}}")) {
    $msg = '✅ Tag *{{bitbucket_tag}}* deployed to *{{target}}* successfully';
  } else {
    $msg = '✅ Branch *{{branch}}* deployed to *{{target}}* successfully'; 
  }
  return $msg;
});
set('googlechat_failure_text',function () {
  $msg = "";
  if (parse("{{bitbucket_tag}}")) {
    $msg = '❌ Tag *{{bitbucket_tag}}* has failed to deploy to *{{target}}*';
  } else {
    $msg = '❌ Branch *{{branch}}* has failed to deploy to *{{target}}*'; 
  }
  return $msg . ": {{build_link}}";
});
set('googlechat_card_title', 'Deployer');

// The message
set('googlechat_message', 'googlechat_notify_text');

// Helpers
desc('Google chat');
task('googlechat_send_message', function(){
  if (!get('googlechat_webhook', false)) {
    return;
  }
  $message_type = get('googlechat_message_type', 'notify');
  $message = get('googlechat_' . $message_type. '_text') . ' at '. date('h:i');

  $data = [
    "text" => "$message",
  ];

  // On ajoute &threadKey=pipeline pour que le message soit posté dans le fil courant et n'en crée pas un nouveau.
  Httpie::post(get('googlechat_webhook').'&threadKey=pipeline')
    ->header('Content-type: application/json; charset=UTF-8')
    ->body($data)
    ->send();
});

// Tasks
desc('Just notify your Google Chat room with all messages, without deploying');
task('googlechat:test', function () {
  set('googlechat_message_type', 'notify');
  invoke('googlechat_send_message');
  set('googlechat_message_type', 'success');
  invoke('googlechat_send_message');
  set('googlechat_message_type', 'failure');
  invoke('googlechat_send_message');
})
  ->once()
  ->shallow();

desc('Notify Google Chat');
task('googlechat:notify', function () {
  set('googlechat_message_type', 'notify');
  invoke('googlechat_send_message');
})
  ->once()
  ->shallow()
  ->setPrivate();

desc('Notify Google Chat about deploy finish');
task('googlechat:notify:success', function () {
  set('googlechat_message_type', 'success');
  invoke('googlechat_send_message');
})
  ->once()
  ->shallow()
  ->setPrivate();

desc('Notify Google Chat about deploy failure');
task('googlechat:notify:failure', function () {
  set('googlechat_message_type', 'failure');
  invoke('googlechat_send_message');
})
  ->once()
  ->shallow()
  ->setPrivate();
