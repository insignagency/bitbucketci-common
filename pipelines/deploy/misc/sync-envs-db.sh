#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-common
#
# Pour debug : set -x
set -e

if [ "$CI_RUN_MODE" != "" ]; then 
  echo "Script exécuté en mode $CI_RUN_MODE."
fi

if [ "$1" == "" ] || [ "$2" == "" ]; then
  echo "Deux arguments sont nécessaires : 1) l'environnement source 2) l'environnement cible"
  echo "Par exemple : prod prep"
  exit 1
fi
src=$1
dest=$2
if [ ! -f "../$src/host.php" ]; then
  echo "Le fichier ../$src/host.php n'existe pas"
  exit 1
fi
if [ ! -f "../$dest/host.php" ]; then
  echo "Le fichier ../$dest/host.php n'existe pas"
  exit 1
fi

infos="$(php get-host-info.php $src)"
# on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done
if [ "$port" == "" ]; then
  port="22"
fi
src_host="$host"
src_port="$port"
src_user="$user"
src_deploy_path="$deploy_path"
src_drupal_site="$drupal_site"

infos="$(php get-host-info.php $dest)"
# on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done

IFSOLD=$IFS
IFS=","
for role in $roles; do
  if [ "$role" == "prod" ] || [ "$role" == "production" ]; then
    echo "Une production ne doit pas être le destinataire d'une synchonisation."
    exit 1
  fi
done
IFS=$IFSOLD

if [ "$port" == "" ]; then
  port="22"
fi
dest_host="$host"
dest_port="$port"
dest_user="$user"
dest_deploy_path="$deploy_path"
dest_drupal_site="$drupal_site"

echo "
#
# Export sql sur l'environnement source ($src)
#
"
scp -P $src_port deployer-mysql-dump.sh $src_user@$src_host:$src_deploy_path/
ssh -p $src_port $src_user@$src_host "chmod +x $src_deploy_path/deployer-mysql-dump.sh"
if [ "$CI_RUN_MODE" != "dry" ]; then 
  ssh -p $src_port $src_user@$src_host "cd $src_deploy_path/; ./deployer-mysql-dump.sh $src_deploy_path $src_deploy_path cd_sync.sql.gz $src_drupal_site"
  scp -P $src_port $src_user@$src_host:$src_deploy_path/cd_sync.sql.gz ./cd_sync.sql.gz
fi

echo "
#
# Import sql sur l'environnement destinataire ($dest) et changement de l'url principale magento en BDD
#
"
echo "-> Upload des éléments sur l'environnements destinataire ($dest)"
scp -P $dest_port ./deployer-mysql-commander.sh $dest_user@$dest_host:
url_to_replace=$(ssh -p $dest_port $dest_user@$dest_host "chmod +x deployer-mysql-commander.sh; ./deployer-mysql-commander.sh $dest_deploy_path get-domain $dest_drupal_site")
if [ "$url_to_replace" != "" ]; then
  echo "base_url trouvée dans la base de données actuelle de $dest : \"$url_to_replace\""
fi
if [ "$CI_RUN_MODE" == "dry" ]; then 
  option="-n"
else
  scp -P $dest_port ./cd_sync.sql.gz $dest_user@$dest_host:$dest_deploy_path/
  option=""
fi
echo "-> Exécution de l'import sur l'environnements destinataire ($dest)"
ssh -p $dest_port $dest_user@$dest_host "./deployer-mysql-commander.sh $option $dest_deploy_path import $dest_deploy_path/cd_sync.sql.gz $dest_drupal_site"
if [ "$url_to_replace" != "" ]; then
  echo "-> Changement des urls dans la base de données de l'environnements destinataire ($dest)"
  ssh -p $dest_port $dest_user@$dest_host "./deployer-mysql-commander.sh $dest_deploy_path update-domain $url_to_replace $dest_drupal_site"
fi

echo "
#
# Nettoyage
#
"
ssh -p $src_port $src_user@$src_host rm $src_deploy_path/deployer-mysql-dump.sh
ssh -p $dest_port $dest_user@$dest_host rm deployer-mysql-commander.sh
if [ "$CI_RUN_MODE" != "dry" ]; then 
 rm ./cd_sync.sql.gz
 ssh -p $dest_port $dest_user@$dest_host rm $dest_deploy_path/cd_sync.sql.gz
 ssh -p $src_port $src_user@$src_host rm $src_deploy_path/cd_sync.sql.gz
fi

