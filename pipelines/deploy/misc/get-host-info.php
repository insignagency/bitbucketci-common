<?php
/*
  Ce fichier provient du repository bitbucketci-common
*/
$host_file_path = dirname(__FILE__)."/../".$argv[1]."/host.php";

get_host_params();

function get_host_params () {
  global $host_file_path;
  if (! file_exists($host_file_path)) {
    echo("Le fichier $host_file_path n'existe pas");
    exit(1);
  }
  $content = shell_exec('php --strip ' . $host_file_path);
  preg_match('|host\([^;]*;|', $content, $matches);
  if (count($matches) == 0) {
    echo("Bloc host(... inexistant ou mal formé");
    echo("Contenu du fichier $host_file_path :");
    echo($content);
    exit(1);
  }
  eval($matches[0]);
}

class Hostinfos {
    function hostname($hostname) {
        printf("\n%s", "hostname=" . $hostname) ;
        return $this;
    }
    function user($user) {
        printf("\n%s", "user=" . $user);
        return $this;
    }
    function port($port) {
        printf("\n%s", "port=" . $port);
        return $this;
    }
    function stage($stage) {
        printf("\n%s", "stage=" . $stage);
        return $this;
    }
    function roles() {
        $values = array();
        foreach(func_get_args() as $arg) {
          $values[] = $arg;
        }
        $roles = implode(",", $values);
        printf("\n%s", "roles=" . $roles);
        return $this;
    }
    function set($key, $value) {
        printf("\n%s", $key . "=" . $value);
        return $this;
    }
}

function host($host) {
  print "host=".$host;
  return new Hostinfos;
}


