#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-common
#
abs_path=$(dirname "$0")
cd $abs_path
if [ "$CI_RUN_MODE" == "debug" ]; then 
  set -x
fi
set -e

if [ "$CI_RUN_MODE" != "" ]; then 
  echo "Script exécuté en mode $CI_RUN_MODE."
fi

### OPTIONS ###
while getopts d: opt; do
  case "$opt" in
  d) export_path=$OPTARG; ;; \?) exit 1 ;; esac
done
shift $(($OPTIND - 1))
### OPTIONS ###

src=$1

if [ "$src" == "" ]; then
  echo "Un argument est nécessaire : 1) l'environnement source"
  echo "Par exemple : prep"
  exit 1
fi

if [ ! -f "../$src/host.php" ]; then
  echo "Le fichier ../$src/host.php n'existe pas"
  exit 1
fi


infos="$(php get-host-info.php $src)"
# on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done
if [ "$port" == "" ]; then
  port="22"
fi
src_host="$host"
src_port="$port"
src_user="$user"
src_deploy_path="$deploy_path"
src_drupal_site="$drupal_site"

if [ "$export_path" == "" ]; then
  export_path=$src_deploy_path
fi

echo "
#
# Export sql sur l'environnement source
#
"
scp -P $src_port deployer-mysql-dump.sh $src_user@$src_host:$export_path/
ssh -p $src_port $src_user@$src_host "chmod +x $export_path/deployer-mysql-dump.sh"

if [ "$CI_RUN_MODE" != "dry" ]; then 
  set -x
  ssh -p $src_port $src_user@$src_host "cd $export_path/; ./deployer-mysql-dump.sh -c $src_deploy_path $export_path cd_sync.sql.gz"
  scp -P $src_port $src_user@$src_host:$export_path/cd_sync.sql.gz ./cd_sync.sql.gz
  set +x
fi

echo "
#
# Import sql sur l'environnement de la CI 
#
"

# on teste si mysql répond sur le host 127.0.0.1 ou mysql
# sur birbucket c'est le 127.0.0.1 sur github sur le host portant le nom du container de service, usuellement nommé mysql
set +e
t=$(mysql -h 127.0.0.1 -u root -pdb_on_docker -e 'show databases;' 2>&1 > /dev/null); 
if [ "$?" -gt 0 ] ; then 
  t=$(mysql -h mysql -u root -pdb_on_docker -e 'show databases;' 2>&1 > /dev/null); 
  if [ "$?" -gt 0 ] ; then 
    echo "Mysql ne répond ni sur le host '127.0.0.1' ni sur le host 'mysql'";
    exit 1
  else
    mysql_host="mysql"
  fi
else
  mysql_host="127.0.0.1"
fi
set -e

if [ "$CI_RUN_MODE" == "dry" ]; then 
  echo "Cette commande aurait été jouée en mode normal :"
  echo "zcat ./cd_sync.sql.gz | mysql -h 127.0.0.1 -u root -pdb_on_docker website"
else
  set -x
  mysql -h $mysql_host -u root -pdb_on_docker -e "SET GLOBAL max_allowed_packet=1073741824;"
  zcat ./cd_sync.sql.gz | sed 's/\\-/-/' | mysql -h $mysql_host -u root -pdb_on_docker website
  echo "Vérification de l'import : listing des tables"
  mysql -h $mysql_host -u root -pdb_on_docker website -e "show tables;"
  set +x
fi

echo "
#
# Nettoyage
#
"
ssh -p $src_port $src_user@$src_host rm $export_path/deployer-mysql-dump.sh
if [ "$CI_RUN_MODE" != "dry" ]; then 
 rm ./cd_sync.sql.gz
 ssh -p $src_port $src_user@$src_host rm $export_path/cd_sync.sql.gz
fi

