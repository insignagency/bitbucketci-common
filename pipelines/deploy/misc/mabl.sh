#!/usr/bin/env bash
#
# Pour voir les valeurs de MABL_APP_ID et MABL_ENV_ID sur mabl, aller à : 
# https://app.mabl.com/workspaces/b41SJRDRljA5w9vucUJm9g-w/settings/apis
# puis API documentation -> Deployment Events Api -> Curl command builder
#
set -e
if [ ! -z "$MABL_API_KEY" ] ; then
  echo "MABL_API_KEY : $(echo $MABL_API_KEY|cut -c1-5)..."
else
  echo "MABL_API_KEY :"
fi
echo "MABL_APP_ID : $MABL_APP_ID"
echo "MABL_ENV_ID : $MABL_ENV_ID"
if [ ! -z "$MABL_API_KEY" ] && [ ! -z "$MABL_APP_ID" ] && [ ! -z "$MABL_ENV_ID" ]; then
  # Launch mabl plans.
  echo "Trigger mabl deployment event"
  curl -s "https://api.mabl.com/events/deployment" \
       -u "key:$MABL_API_KEY" \
       -H "Content-Type:application/json" \
       -d "{\"environment_id\":\"$MABL_ENV_ID\",\"application_id\":\"$MABL_APP_ID\"}"
else
  echo "Tests Mabl non lancés car pas de variables MABL_XXX configurés."

fi