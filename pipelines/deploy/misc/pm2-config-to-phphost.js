env = process.argv[2]

var configs = require(`../${env}/pm2-deploy.config.js`);

let config = configs.deploy[env]

if(config.host){
	host = config.host
	port = "22"
}
if(config.host.host){
	host = config.host.host
	if(config.host.port){
	  port = config.host.port
	} else {
		port = "22"
	}
}
user = config.user
if(config.path){
	deployPath = config.path
}


var template = `
<?php
host('HOST')
	->port('PORT')
	->user('USER')
	->stage('STAGE')
	->set('deploy_path', 'DEPLOY_PATH')
	->roles('ROLE') 
	;
`
template = template.replace('HOST', host)
template = template.replace('PORT', port)
template = template.replace('USER', user)
template = template.replace('STAGE', env)
template = template.replace('DEPLOY_PATH', deployPath)
template = template.replace('ROLE', env)
console.log(template)