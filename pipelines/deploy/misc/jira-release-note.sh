#!/usr/bin/env bash
#
# Ce script passe une version jira en statut "released"
# Cela déclenche un module Jira qui va publier la release note dans le dossier releases/ de l'espace Confluence du projet
#
set -e
echo "JIRA_VERSION_TAG=$JIRA_VERSION_TAG"

if [ ! -z "$JIRA_VERSION_TAG" ] ; then
  set +e
  version_id=$(curl -s --request GET \
    --url "https://insign-agency.atlassian.net/rest/api/2/project/BANA/version?query=$JIRA_VERSION_TAG" \
    --user "$JIRA_API_USER:$JIRA_API_TOKEN" \
    --header 'Accept: application/json' |python3 -c 'import sys, json; print(json.load(sys.stdin)["values"][0]["id"])')

  set -e
  if [[ "$version_id" =~ v[0-9] ]]; then
  curl --request PUT \
    --url "https://insign-agency.atlassian.net/rest/api/2/version/$version_id" \
    --user "$JIRA_API_USER:$JIRA_API_TOKEN" \
    --header 'Accept: application/json' \
    --header 'Content-Type: application/json' \
    --data '{"released": true}'
  else
    echo "Aucune version JIRA trouvée pour la valeur $JIRA_VERSION_TAG."
  fi
   
else
  echo "Pas de variable JIRA_VERSION_TAG définie : on ne crée pas de release note."
fi