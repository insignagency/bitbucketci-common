#!/bin/bash
#
#  Ce fichier provient du repository bitbucketci-common
#
# Pour debug : set -x
set -e

if [ "$CI_RUN_MODE" != "" ]; then 
  echo "Script exécuté en mode $CI_RUN_MODE."
fi

if [ "$1" == "" ] || [ "$2" == "" ] || [ "$3" == "" ]; then
  echo "Trois arguments sont nécessaires : 1) l'environnement source 2) l'environnement cible 3) le chemin à synchroniser"
  echo "Par exemple : prod prep shared/pub"
  exit 1
fi
src=$1
dest=$2
path=$3
if [ ! -f "../$src/host.php" ]; then 
  echo "Le fichier ../$src/host.php n'existe pas"
  exit 1
fi
if [ ! -f "../$dest/host.php" ]; then 
  echo "Le fichier ../$dest/host.php n'existe pas"
  exit 1
fi


infos="$(php get-host-info.php $src)"
# on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done
if [ "$port" == "" ]; then
  port="22"
fi
src_host="$host"
src_port="$port"
src_user="$user"
src_deploy_path="$deploy_path"

infos="$(php get-host-info.php $dest)"
# on n'exporte pas les variables qui ont un nom interdit, contenant un slash : bin/php=php7.4
for var_exp in $infos; do
    if [[ ! "$var_exp" =~ .+/.+= ]]; then
    eval $var_exp
  fi
done
IFSOLD=$IFS; IFS=","
for role in $roles
do
  if [ "$role" == "prod" ] || [ "$role" == "production" ]; then
    echo "Une production ne doit pas être le destinataire d'une synchonisation."
    exit 1
  fi
done
IFS=$IFSOLD
if [ "$port" == "" ]; then
  port="22"
fi
dest_host="$host"
dest_port="$port"
dest_user="$user"
dest_deploy_path="$deploy_path"

echo "
#
# Vérification/installation clé pub de la source vers la destination
#
"
set +e
pubkey=$(ssh -p $src_port $src_user@$src_host cat .ssh/id_rsa.pub  2>/dev/null)

# pas de clé, on en fabrique une
if [ "$?" -gt 0 ]; then
  echo "Pas de clé rsa pour le user $src_user sur $src_host. On en génère une."
  ssh -p $src_port $src_user@$src_host "ssh-keygen -t rsa -N '' -f .ssh/id_rsa"
fi
pubkey=$(ssh -p $src_port $src_user@$src_host cat .ssh/id_rsa.pub  2>/dev/null)
dest_authfile_content=$(ssh -p $dest_port $dest_user@$dest_host cat .ssh/authorized_keys 2>/dev/null)

if [ "$(echo "$dest_authfile_content" | grep -n "$pubkey")" == "" ]; then
  echo "La clé publique pour le user $src_user@$src_host n'est pas présent sur $dest_user@$dest_host. On l'ajoute."
  echo "Adding source pubkey in dest user authorized_keys file"
  ssh -p $dest_port $dest_user@$dest_host "echo \"$pubkey\" >> .ssh/authorized_keys"
fi

set -e
echo "
#
# Définition relation ip/port entre soucre et destination : pub à pub ou local à local
#
"
src_local_first_ip=$(ssh -p $src_port $src_user@$src_host "hostname -I |cut -d ' ' -f1")
dest_local_first_ip=$(ssh -p $dest_port $dest_user@$dest_host "hostname -I |cut -d ' ' -f1")

if [ "$(echo $src_local_first_ip | cut -d '.' -f1-3)" == "$(echo $dest_local_first_ip | cut -d '.' -f1-3)" ]; then
  # on vérifie si ça communique bien en tcp via lan sur le port 22
  set +e
  ssh -p $src_port $src_user@$src_host "timeout 2 bash -c \"</dev/tcp/$dest_local_first_ip/22\"" 2>/dev/null
  if [ "$?" -eq 0 ]; then 
    echo "Local à local."
    rsync_dest_co="-e 'ssh -o 'StrictHostKeyChecking=no' -p 22' $dest_user@$dest_local_first_ip"
  fi
fi
  # sinon on passe par le wan
if [ "$rsync_dest_co" == "" ]; then
 echo "Pub à pub"
 rsync_dest_co="-e 'ssh -o 'StrictHostKeyChecking=no' -p $dest_port' $dest_user@$dest_host"
fi
set -e

cmd="ssh -p $src_port $src_user@$src_host \"rsync -rv --checksum $src_deploy_path/$path/ $rsync_dest_co:$dest_deploy_path/$path/\""
if [ "$CI_RUN_MODE" == "dry" ]; then
 echo "Dry run, voici la commande qui aurait été lancée en mode normal :"
 echo "$cmd" 
else
 eval $cmd
 echo "Synchronisation effectuée. Voici la commande executée : "
 echo "$cmd" 
fi



